var Register = function () {

    this.enterEmailForRegistration = function (data) {
        element(by.model('register.email')).sendKeys(data);
    }

    this.enterPwdForRegistration = function (data) {
        element(by.model('register.password')).sendKeys(data);
    }

    this.reEnterPwdForRegistration = function (data) {
        element(by.model('register.re_password')).sendKeys(data);
    }

    this.selectBuyercheckbox = function () {
        $('[id="checkbox1"]').click();
    }

    this.selectSupplierCheckbox = function () {
        $('[id="checkbox2"]').click();
    }

    
    this.clickOnSelectWhichEverApplicable=function(){
        element(by.className('dropdown-toggle ng-binding btn btn-default')).click();
        element(by.id('selectAll')).click();
    }

    this.selectTermsAndConditions = function () {
        // $('[id="checkboxTnC"]').click();
        element(by.model('acceptTnC')).click();
    }

    this.clickOnRegistration = function () {
        // element(by.css('.login-box-submit-button')).click();
        element(by.id('register')).click();
    }


    this.get = async function () {
        // browser.get('http://localhost/register/default/step2?userId=82');
        browser.sleep(2000);
        browser.get('http://localhost/register/default/step2?userId=93');
        browser.sleep(5000);
    };



}

module.exports = new Register;