var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Loginregister = require(__dirname + '/login-register.page.js');
var Register = require(__dirname + '/../../register/register.page.js');


describe("Register from Login-Page", function () {

    it("Opens Supplierscave Application", function () {
        Getstarted.loginGet();
    })

    it("Click on New User registration", function () {
        Loginregister.clickOnRegisterInLoginPage();
    })

    it("Enter Email for new Registration", function () {
        Register.enterEmailForRegistration('livelikeme1986+buyer270720182@gmail.com');
    })

    it("Enter password for new Registration", function () {
        Register.enterPwdForRegistration('supplierscave');
    })

    it("Re-Enter password for new Registration", function () {
        Register.reEnterPwdForRegistration('supplierscave');
    })

    it("Select Buyer Dashboard", function () {
        Register.selectBuyercheckbox();
    })

    it("Select Supplier Dashboard", function () {
        Register.selectSupplierCheckbox();
    })

    it("Select I agree to Terms & Conditions", function () {
        Register.selectTermsAndConditions();
    })

    it("Select Which ever Applicatble", function () {
        Register.clickOnSelectWhichEverApplicable();
    })

    it("Click on Register for registration", function () {
        Register.clickOnRegistration();
    })

    it("Open additional Information Browser", function () {
        Register.get();
    })

})

