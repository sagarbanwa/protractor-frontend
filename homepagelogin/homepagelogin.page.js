var Homepagelogin = function(){

    this.userName=function(data){
        element(by.id('userNameHomePage')).sendKeys(data);
    }
 
    this.userPwd=function(data){
        element(by.id('login-password')).sendKeys(data);
    }
     
    this.userLogin=function(){
        element(by.id('homePageLogin')).click();
    }
}
module.exports = new  Homepagelogin;