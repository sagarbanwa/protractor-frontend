var Dashboard = require(__dirname + '/../dashboard.page.js');
var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js')
var Admindashboard = require(__dirname + '/admin-dashboard.page.js');
var Home = require(__dirname+'/../../homepage/homepage.page.js');

describe("Admin Dashboard functionality", function () {

    it("Login into Admin Dashboard ", function () {
        Getstarted.get();
    });

    it("Enter Admin UserName", function () {
        Homepagelogin.userName('livelikeme1986+admin27072018@gmail.com');
    })

    it("Enter Admin Password", function () {
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login", function () {
        Homepagelogin.userLogin();
    })

    it("Click on Dashboard Actions in Application", function () {
        Dashboard.clickActions();
    })

    it("Click on Admin Dashboard", function () {
        Dashboard.clickAdminDashboard();
    })

    

    // it("Click on Suppliers", function () {
    //     Admindashboard.clickOnSuppliers();
    // })

    // it("Click on Inventories and Suppliers", function () {
    //     Admindashboard.clickOnInventoriesAndRental();
    // })

    // it("Click on Product and Services in directory", function () {
    //     Admindashboard.clickOnProductAndServicesInDirectory();
    // })

    // it("Click on Supplier location & Contact Details", function () {
    //     Admindashboard.clickOSupplierLocationAndContactDetails();
    // })

    // it("Click on Manufacturers", function () {
    //     Admindashboard.clickOnManufacturers();
    // })

    // it("Click On Industries", function () {
    //     Admindashboard.clickOnIndustries();
    // })

    // it("Click on Parent Category", function () {
    //     Admindashboard.clickOnParentCategories();
    // })

    // it("Click on Category", function () {
    //     Admindashboard.clickOncategory();
    // })

    // it("Click on Location", function () {
    //     Admindashboard.clickOnLocation();
    // })

    // it("Click On Currency", function () {
    //     Admindashboard.clickOnCurrency();
    // })

    // it("Click on Content",function(){
    //     Admindashboard.clickOnContent();
    // })

    // it("Click On Newsletter",function(){
    //     Admindashboard.clickOnNewsletter();
    // })

    // it("Click on Suppliers Logo",function(){
    //     Admindashboard.clickOnSiteLogo();
    // })

    // it("Logout form Admin Dashboard",function(){
    //     Dashboard.clickLogout();
    // })

    // it("Scroll Down the page",function(){
    //     Admindashboard.clickActions();
    // })

    // it("Click on Content", function () {
    //     Admindashboard.content();
    // })

});