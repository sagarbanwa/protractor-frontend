var Adminsuppliers = function () {

    this.clickOnAddSupplier= function(){
        element(by.id('add-supplier')).click();
    }

    this.clickOnAddMultipleSuppliers=function(){
        element(by.id('add-multiple-suppliers')).click();
    }

    this.enterCompanyDetails = function (data) {
        element(by.id('Companyname')).sendKeys(data);
    }

    this.clearWebAddress = function () {
        var foo = element(by.id('Webaddress'));
        expect(foo.getAttribute('id')).toEqual('Webaddress');
        foo.clear();
    }
    // var foo = element(by.id('foo'));
    // expect(foo.getAttribute('class')).toEqual('bar');
    this.enterWebAddress = function (data) {
        element(by.id('Webaddress')).sendKeys(data);
    }

    // this.selectLogo=function(){
    //     element(by.id('Logo')).click();
    // }

    // this.uploadLogo=function(){
    //     browser.sleep(1000);
    //     element(by.id('company-logo')).click();
    //     browser.sleep(1000);
    // }

    this.uploadLogo = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/admin-dashboard/suppliers/add-supplier/miltion-logo.png';
        absolutePath = path.resolve(__dirname, fileToUpload);
        // // element(by.class('ng-isolate-scope')).sendKeys(absolutePath);
        // browser.sleep(3000);
        // element(by.css('input[type="file"]')).sendKeys(absolutePath);    
        //    element(by.xpath("//input[@ng-multi-file-model='productImages']")).sendKeys(absolutePath);
        element(by.id('Logo')).sendKeys(absolutePath);
        // element(by.css('input[type="file"]')).click();
        // browser.sleep(5000);
        element(by.id('company-logo')).click();
        // browser.sleep(15000);
    };

    this.selectEstablishmentType = function (data) {
        element(by.id('establishmentType')).click();
        element(by.id('establishmentType')).sendKeys(data);
    }

    this.companyType = function (data) {
        element(by.id('CompanyType')).click();
        element(by.id('CompanyType')).sendKeys(data);
    }

    this.selectSupplierCheckbox = function () {
        $('[id="Supplier"]').click();
    }

    this.selectBuyerCheckbox = function () {
        browser.sleep(2000);
        $('[id="Buyer"]').click();
        browser.sleep(2000);
    }

    this.clickCompanyProfile = function (data) {
        element(by.id('company-profile')).click();
        element(by.id('company-profile')).sendKeys(data);
    }

    this.documentLicense = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/manage-company/Inventory_Upload_Format_Supplier.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        element(by.id('incorporationdocument')).sendKeys(absolutePath);
        element(by.id('companydata-attachments')).click();
    }

    this.clearEmployees=function(){
        var foo=element(by.model('companyData.num_employees'));
        expect(foo.getAttribute('ng-model')).toEqual('companyData.num_employees');
        foo.clear();
    }

    this.employees=function(data){
        element(by.model('companyData.num_employees')).sendKeys(data);
    }

    this.aboutUs=function(data){
        element(by.id('ui-tinymce-4_ifr')).click();
        // browser.sleep(1000);
        element(by.id('ui-tinymce-4_ifr')).sendKeys(data);
        // browser.sleep(1000);
    }

    this.sendEnquiryMessage = function (data) {
        element(by.id('ui-tinymce-1_ifr')).sendKeys(data);
    }

    this.aboutUsImage=function(){
        var path=require('path');
        var fileToUpload='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/admin-dashboard/suppliers/add-supplier/milton-1.jpeg';
        absolutePath=path.resolve(__dirname,fileToUpload);
        element(by.id('company-image-about-us')).sendKeys(absolutePath);
        element(by.id('company-image-upload-images')).click();     
    }

    this.catalogImages=function(){
        var path=require('path');
        var fileToUpload='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/admin-dashboard/suppliers/add-supplier/milton.png';
        absolutePath=path.resolve(__dirname,fileToUpload);
        element(by.id('company-image-catalog_images')).sendKeys(absolutePath);
        element(by.id('catalog-image-upload-images')).click();       
    }

    this.addAddress=function(){
        element(by.id('add-address')).click();
    }
    
    this.nameOfAddress=function(data){
        element(by.id('nameofaddress')).sendKeys(data);
    }

    this.address1=function(data){
        element(by.id('addressline1')).sendKeys(data);
    }

    this.address2=function(data){
        element(by.id('addressline2')).sendKeys(data);
    }

    this.city=function(data){
        element(by.id('City')).sendKeys(data);
    }

    this.state=function(data){
        element(by.id('State')).sendKeys(data);
    }

    this.country=function(data){
        element(by.id('Country')).sendKeys(data);
    }

    this.googleMapsUrl=function(data){
        element(by.id('MapUrl')).sendKeys(data);
    }

    this.countryCode1=function(data){
        element(by.id('countryCode10')).sendKeys(data);
    }

    this.countryCode2=function(data){
        element(by.id('countryCode20')).sendKeys(data);
    }

    this.phoneNumber1=function(data){
        element(by.id('phonenumber10')).sendKeys(data);
    }

    this.phoneNumber2=function(data){
        element(by.id('phonenumber20')).sendKeys(data);
    }

    this.emailId1=function(data){
        element(by.model('contact.emailid1')).sendKeys(data);
    }

    this.emailId2=function(data){
        element(by.id('emailid2')).sendKeys(data);
    }

    this.facebookUrl=function(data){
        element(by.model('contact.facebook_url')).sendKeys(data);
    }

    this.twitterUrl=function(data){
        element(by.id('twitter_url')).sendKeys(data);
    }

    this.linkedInUrl=function(data){
        element(by.model('contact.linkedin_url')).sendKeys(data);
    }

    this.youtubeUrl=function(data){
        element(by.model('contact.youtube_url')).sendKeys(data);
    }

    this.previewCompanyPage=function(){
        element(by.id('preview-company-page')).click();
    }

    this.saveCompanyInfo=function(){
        browser.sleep(5000);
        element(by.id('save-company-info')).click();
        browser.sleep(5000);
    }



}
module.exports = new Adminsuppliers;