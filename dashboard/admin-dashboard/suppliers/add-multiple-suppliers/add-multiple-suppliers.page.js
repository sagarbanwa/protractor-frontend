var Addmultiplesuppliers = function () {

    this.clickOnAddMultipleSuppliers = function () {
        browser.sleep(1000);
        element(by.id('add-multiple-suppliers')).click();
        browser.sleep(2000);
    }

    this.uploadSupplierFile = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/admin-dashboard/suppliers/add-multiple-suppliers/Supplier info - Sheet1.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(2000);
        element(by.id('multiple-supplier-file-attachement')).sendKeys(absolutePath);
        // element(by.id('companydata-attachments')).click();
        browser.sleep(2000);
    }    

    this.multipleSupplierSaveChanges=function(){
        element(by.id('mulitple-supplier-save-changes')).click();
    }


}
module.exports = new Addmultiplesuppliers;