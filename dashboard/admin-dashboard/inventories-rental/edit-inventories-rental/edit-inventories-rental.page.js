var Editinventoriesrentalpage = function () {

    this.clickOnEdit = function () {
        // browser.sleep(1000);
        element(by.id('edit')).click();
    }

    this.selectSubCategory = function () {
        // browser.sleep(2000);
        // element(by.id('selected-category')).click();
        // element(by.id('select-role-type')).click();
        // element(by.xpath('//text()[contains(.,"1 checked")]/ancestor::button[1]')).click();
        // element(by.xpath('//*[@id="select-role-type"]')).click();
        // element(by.model('selectedCategory')).click();
        // browser.sleep(2000);
        element(by.xpath('/html[1]/body[1]/div[1]/section[1]/div[3]/div[1]/ui-view[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]')).click();
        // element(by.id('selected-category')).sendKeys(data);
        // element(by.xpath('//*[@id="selected-category"]/div/ul/li[5]/a/span[2]')).click();
        element(by.xpath('//span[text()="Chemicals"]')).click();
        // browser.sleep(1000);
    }

    this.selectsSelectedSubCategory = function () {
        element(by.id('selected-sub-category')).click();
        element(by.xpath('/html[1]/body[1]/div[1]/section[1]/div[3]/div[1]/ui-view[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]')).click();
        browser.sleep(1000);

        // element(by.xpath('//*[@id="selected-sub-category"]/div/ul/li[6]/a')).click();
        element(by.xpath('//span[text()="Chemicals"]')).click();
        browser.sleep(1000);

    }

    this.selectSelectedSecondarySubCategory = function () {
        element(by.id('selected-secondary-sub-category')).click();
        browser.sleep(1000);
        // element(by.xpath('//*[@id="selected-secondary-sub-category"]/div/ul/li[5]/a/span[2]')).click();
        element(by.xpath('//span[text()="Organic Pigments"]')).click();
        browser.sleep(1000);
    }

    this.selectSelectedTeritarySubCategory = function () {
        element(by.id(selected - teritary - sub - category)).click();
    }


    this.selectCountries = function () {
        element(by.id('select-countries')).click();
        browser.sleep(2000);
        // element(by.xpath('//text()[contains(.,"Canada")]/ancestor::a[1]')).sendKeys(data);
        element(by.xpath('//span[text()="Canada"]')).click();
        browser.sleep(1000);
        element(by.xpath('//span[text()="India"]')).click();
        browser.sleep(2000);
    }

    this.selectState = function () {
        element(by.id('select-states')).click();
        browser.sleep(2000);
        element(by.xpath('//span[text()="Ontario"]')).click();
        element(by.xpath('//span[text()="Maharashtra"]')).click();
        browser.sleep(2000);
    }

    this.applyFilters = function () {
        browser.sleep(5000);
        element(by.id('apply-filters')).click();
        browser.sleep(5000);
    }

    this.clear = function () {
        browser.sleep(10000);
        element(by.id('clear')).click();
        browser.sleep(10000);
    }
}
module.exports = new Editinventoriesrentalpage;