// var Getstarted = require(__dirname + '/../../../../getstarted/getstarted.page.js');
var Admindashboard = require(__dirname + '/../../../../dashboard/admin-dashboard/admin-dashboard.page.js');
var Viewinventoriesrental = require(__dirname + '/view-inventories-rental.page.js');

describe("View Inventories and Rental as a Admin", function () {

    it("Select Inventories and Rental", function () {
        Admindashboard.clickOnInventoriesAndRental();
    });

    it("Select Category", function () {
        Viewinventoriesrental.selectSubCategory();
    });

    it("Select sub Category",function(){
        Viewinventoriesrental.selectsSelectedSubCategory();
    })

    it("Select secondary sub category",function(){
        Viewinventoriesrental.selectSelectedSecondarySubCategory();
    })

    it("Select country",function(){
        Viewinventoriesrental.selectCountries();
    })

    it("Select State",function(){
        Viewinventoriesrental.selectStates();
    })

    it("Select Cities",function(){
        Viewinventoriesrental.selectCities();
    })

    it("Apply Filters",function(){
        Viewinventoriesrental.applyFilters();
    })

    it("Clear Filters",function(){
        Viewinventoriesrental.clear();
    })

})
