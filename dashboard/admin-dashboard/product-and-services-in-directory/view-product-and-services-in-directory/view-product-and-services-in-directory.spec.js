var Viewproductandservicesindirecoty = require(__dirname + '/view-product-and-services-in-directory.page.js');
var Admindashboard = require(__dirname + '/../../../../dashboard/admin-dashboard/admin-dashboard.page.js');


describe("Product and Services in directory View", function () {

    it("Click on Product and Services in directory view", function () {
        Admindashboard.clickOnProductAndServicesInDirectory();
    })

    it("Select sub Category", function () {
        Viewproductandservicesindirecoty.selectsSelectedSubCategory();
    })

    it("Select secondary sub category", function () {
        Viewproductandservicesindirecoty.selectSelectedSecondarySubCategory();
    })

    it("Select country", function () {
        Viewproductandservicesindirecoty.selectCountries();
    })

    it("Select State", function () {
        Viewproductandservicesindirecoty.selectStates();
    })

    it("Select Cities", function () {
        Viewproductandservicesindirecoty.selectCities();
    })

    it("Apply Filters", function () {
        Viewproductandservicesindirecoty.applyFilters();
    })

    it("Clear Filters", function () {
        Viewproductandservicesindirecoty.clear();
    })




});
