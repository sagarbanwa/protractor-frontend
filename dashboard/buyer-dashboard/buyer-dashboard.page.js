var Buyerdashboard = function () {

    this.clickShorlists = function () {
        element(by.id("buyerShortlist")).click();
    }

    this.clickMessages = function () {
        element(by.id("buyerMessages")).click();
    }

    this.clickOnSiteLogo=function(){
        element(by.id('site-logo')).click();
    }
}
module.exports = new Buyerdashboard;