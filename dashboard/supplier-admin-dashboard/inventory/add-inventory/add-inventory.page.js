var Addinventory = function () {

    this.addInventory = function () {
        element(by.id('add-inventory')).click();
    }

    this.addMulitpleInventory = function () {
        element(by.id('add-multiple-inventories')).click();
    }

    this.addTitleToInventory = function (data) {
        element(by.model('inventoryData.title')).sendKeys(data);
    }

    this.addDescriptionToInventory = function (data) {
        element(by.model('inventoryData.description')).sendKeys(data);
    }

    this.selectInventoryCategory = function (data) {
        element(by.id('ex3_value')).click();
        element(by.id('ex3_value')).sendKeys(data);
        element(by.xpath("//div[@class='angucomplete-title ng-binding ng-scope']")).click();
    }

    this.selectSubInventory = function (data) {
        element(by.model('inventoryData.sub_category')).click();
        element(by.model('inventoryData.sub_category')).sendKeys(data);
    }

    this.selectSecondarySubCategory = function (data) {
        element(by.model('inventoryData.sub_sub_category')).click();
        element(by.model('inventoryData.sub_sub_category')).sendKeys(data);
    }

    this.totalQuantity = function (data) {
        element(by.id('TotalQuantity')).sendKeys(data);
    }

    this.quantitySold = function (data) {
        element(by.id('QuantitySold')).sendKeys(data);
    }

    this.availableQuantity = function (data) {
        element(by.id('Available Quantity')).sendKeys(data);
    }

    this.addMaterial = function (data) {
        element(by.model('inventoryData.material')).sendKeys(data);
    }

    this.addGrade = function (data) {
        element(by.model('inventoryData.grade')).sendKeys(data);
    }

    this.addStandard = function (data) {
        element(by.model('inventoryData.standard')).sendKeys(data);
    }

    this.selectCountry = function (data) {
        element(by.id('ex1_value')).click();
        element(by.id('ex1_value')).sendKeys(data);
        element(by.xpath('//div[@class="angucomplete-title ng-binding ng-scope"]')).click();
    }

    this.selectState = function (data) {
        element(by.model('inventoryData.state')).click();
        element(by.model('inventoryData.state')).sendKeys(data);
    }

    this.selectCity = function (data) {
        element(by.model('inventoryData.city')).click();
        element(by.model('inventoryData.city')).sendKeys(data);
    }

    this.addManufacturer = function (data) {
        element(by.model('inventoryData.manufacturer')).sendKeys(data);
    }

    this.selectIndustry = function () {
        element(by.id('inventory-selected-industry')).click();
        element(by.id("selectAll")).click();
    }

    this.enterRemarks = function (data) {
        element(by.model('inventoryData.remarks')).sendKeys(data);
    }

    this.uploadImages = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-inventory/inventory.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(2000);
        element(by.id('inventory-product-images')).sendKeys(absolutePath);
        browser.sleep(2000);
    };

    this.uploadImages1= function(){
        var path= require('path');
        var fileToUpload='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-inventory/inventory-1.jpeg';
        absolutePath =path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('inventory-product-images')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadImages2=function(){
        var path = require('path');
        var fileToUpload='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-inventory/inventory-2.jpeg';
        absolutePath= path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('inventory-product-images')).sendKeys(absolutePath);
        browser.sleep(1000);
    }



    this.uploadDocuments = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-inventory/GeneralCableKSA-1.csv';
        browser.sleep(2000);
        absolutePath = path.resolve(__dirname, fileToUpload);
        element(by.id('inventory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(2000);

    };

    this.uploadDocuments1=function(){
        var path = require('path');
        var fileToUpload ='/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/inventory/add-inventory/test .csv';
        absolutePath=path.resolve(__dirname,fileToUpload);
        browser.sleep(1000);
        element(by.id('inventory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.saveInventory = function () {
        browser.sleep(2000);
        element(by.id('save-inventory')).click();
        browser.sleep(2000);
    }
}

module.exports = new Addinventory;