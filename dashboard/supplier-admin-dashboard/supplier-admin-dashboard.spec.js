var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js')
var Dashboard = require(__dirname+'/../../dashboard/dashboard.page.js')
// var Supplieradmindashboard = require(__dirname+'/supplier-admin-dashboard.page.js')

describe('Supplier Admin Functionality', function () {

    it("Login into Supplier Admin Dashboard", function () {
        Getstarted.get();
    });

    it("Enter Username for Supplier Admin", function () {
        Homepagelogin.userName('livelikeme1986+protractorsupplieradmin@gmail.com');
    })

    it("Enter User Password for Supplier Admin", function () {
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login", function () {
        Homepagelogin.userLogin();
    })

    it("MouseOver to  Supplier Admin",function(){
        Dashboard.clickActions();
    })

    it("Click on Supplier Admin",function(){
        Dashboard.clickSuppliersDashboard();
    })

    
});

