var Addteam= function(){

    this.clickOnTeam=function(){
        element(by.id('supplier-team')).click();
    }

    this.addTeamMembers=function(){
        element(by.id('add-team-members')).click();
        browser.sleep(5000);
    }

    this.addMemberName=function(data){
        element(by.model('user.name')).sendKeys(data);
    }

    this.addMemberEmail=function(data){
        element(by.model('user.email')).sendKeys(data);
    }

    this.sendTeamRegistration=function(){
        browser.sleep(2000);
        element(by.id('send-team-registration')).click();
        browser.sleep(5000);
    }
}
module.exports=new Addteam;