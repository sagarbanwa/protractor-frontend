var Addmultipledirectories = require(__dirname + '/add-multiple-directory.page.js');

describe("Add multiple directories ", function () {

    it("Click On Product & Services in directory ", function () {
        Addmultipledirectories.clickOnProductAndServicesInDirectory();
    })

    it("Click On Add Multiple Directories", function () {
        Addmultipledirectories.clickOnMultipleProduct();
    })

    it("Select Industry Type",function(){
        Addmultipledirectories.selectIndustryType();
    })

    it("Download Directory Template",function(){
        Addmultipledirectories.downloadDirectoryTemplate();
    })

    it("Click on upload Image for mulitple directory",function(){
        Addmultipledirectories.uploadImages();
    })

    it("Upload Product Image1 for multiple directory ",function(){
        Addmultipledirectories.uploadImages1();
    })

    it("Upload Product Image2 for multiple directory",function(){
        Addmultipledirectories.uploadImages2();
    })

    it("Add Documents1 for multiple directory",function(){
        Addmultipledirectories.uploadDocuments();
    })

    it("Add Documents2 for multiple directory",function(){
        Addmultipledirectories.uploadDocuments1();
    })

    it("Add Products for multiple directory",function(){
        Addmultipledirectories.uploadFiles();
    })

    it("Save Multiple Directory",function(){
        Addmultipledirectories.saveMultipleDirectory();
    })



})


