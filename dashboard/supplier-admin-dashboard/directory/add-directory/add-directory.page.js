var Adddirectory = function () {

    this.clickOnProductAndServicesInDirectory = function () {
        element(by.id('supplier-directory')).click();
    }

    this.addDirectory = function () {
        element(by.id('add-product')).click();
    }

    this.addTitle = function (data) {
        element(by.model('productData.title')).sendKeys(data);
    }

    this.addDescription = function (data) {
        element(by.model('productData.description')).sendKeys(data);
    }

    this.selectCategory = function (data) {
        element(by.id('ex2_value')).click();
        element(by.id('ex2_value')).sendKeys(data);
        element(by.xpath('//div[@class="angucomplete-title ng-binding ng-scope"]')).click();
    }

    this.selectSubCategory = function (data) {
        element(by.model('productData.sub_category')).click();
        element(by.model('productData.sub_category')).sendKeys(data);
    }

    this.selectSecondarySubCategory = function (data) {
        element(by.model('productData.sub_sub_category')).click();
        element(by.model('productData.sub_sub_category')).sendKeys(data);
    }

    this.selectTertiarySubCategory = function (data) {
        element(by.model('productData.sub_sub_sub_category')).click();
        element(by.model('productData.sub_sub_sub_category')).sendKeys(data);
    }

    this.prodcutMaterial = function (data) {
        element(by.model('productData.material')).sendKeys(data);
    }

    this.productGrade = function (data) {
        element(by.model('productData.grade')).sendKeys(data);
    }

    this.productStandard = function (data) {
        element(by.model('productData.standard')).sendKeys(data);
    }

    this.productCountry = function (data) {
        element(by.id('ex1_value')).click();
        element(by.id('ex1_value')).sendKeys(data);
        element(by.xpath('//div[@class="angucomplete-title ng-binding ng-scope"]')).click();
    }

    this.productState = function (data) {
        element(by.model('productData.state')).click();
        element(by.model('productData.state')).sendKeys(data);
    }

    this.productCity = function (data) {
        element(by.model('productData.city')).click();
        element(by.model('productData.city')).sendKeys(data);
    }

    this.productManufacturer = function (data) {
        element(by.model('productData.manufacturer')).sendKeys(data);
    }

    this.productIndustry = function () {
        element(by.id('product-selected-industry')).click();
        element(by.id('selectAll')).click()

    }

    this.productRemarks = function (data) {
        element(by.id('Remarks')).sendKeys(data);
    }

    this.uploadImages = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/directory.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(2000);
        element(by.id('directory-product-images')).sendKeys(absolutePath);
        browser.sleep(2000);
        // element(by.id('directory-upload-images')).click();
    };

    this.uploadImages1 = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/directory-1.jpeg';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('directory-product-images')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadImages2 = function () {
        var path = require('path');
        var fileToUpload = "/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/directory-2.jpeg";
        absolutepath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('directory-product-images')).sendKeys(absolutepath);
        browser.sleep(1000);
    }

    this.uploadDocuments = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/Valmark.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        // // element(by.class('ng-isolate-scope')).sendKeys(absolutePath);
        // element(by.css('input[type="file"]')).sendKeys(absolutePath);    
        // element(by.xpath("//input[@ng-multi-file-model='fileAttachments']")).sendKeys(absolutePath);
        // browser.sleep(2000);
        element(by.id('directory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(2000);
        // // element(by.css('input[type="file"]')).click();
        // element(by.id('directory-upload-files')).click();

    };

    this.uploadDocuments1 = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/Directory-1.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('directory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.uploadDocuments2 = function () {
        var path = require('path');
        var fileToUpload = "/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/Directory-2.csv";
        absolutePath = path.resolve(__dirname, fileToUpload);
        browser.sleep(1000);
        element(by.id('directory-file-attachments')).sendKeys(absolutePath);
        browser.sleep(1000);
    }

    this.saveDirectory = function () {
        browser.sleep(2000);
        element(by.id('save-directory')).click();
        browser.sleep(2000);
    }


}
module.exports = new Adddirectory;