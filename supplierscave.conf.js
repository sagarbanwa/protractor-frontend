exports.config = {
  //The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  capabilities: {
    'browserName': 'chrome'
  },
  framework: 'jasmine',

  suites: {

    homepage: ['./homepage/homepage.spec.js'],
    dashboard: ['./dashboard/dashboard.spec.js'],
    admindashboard: ['./dashboard/admin-dashboard/admin-dashboard.spec.js'],
    adminusers: ['./dashboard/admin-dashboard/users/view-users/view-users.spec.js'],
    adminaddsupplier: ['./dashboard/admin-dashboard/suppliers/add-supplier/add-supplier.spec.js'],
    adminaddmultiplesupplier: ['./dashboard/admin-dashboard/suppliers/add-multiple-suppliers/add-multiple-suppliers.spec.js'],
    adminviewinvertoryrental:['./dashboard/admin-dashboard/inventories-rental/view-inventories-rental/view-inventoires-rental.spec.js'],
    admineditinventoryrental: ['./dashboard/admin-dashboard/inventories-rental/edit-inventories-rental/edit-inventories-rental.spec.js'],
    adminviewproductandservicesindirectory:['./dashboard/admin-dashboard/product-and-services-in-directory/view-product-and-services-in-directory.spec.js'],
    supplierDashboard: ['./dashboard/supplier-dashboard/supplier-dashboard.spec.js'],
    buyerDashboard: ['./dashboard/buyer-dashboard/buyer-dashboard.spec.js'],
    register: ['./register/register.spec.js'],
    registerAdditionalInformation: ['./registeradditionalinformation/register-additional-information.spec.js'],
    editaccountsettings: ['./editaccountsettings/edit-account-settings.spec.js'],
    forgotpassword: ['./forgotpassword/forgot-password.spec.js'],
    // login: ['./login/login.spec.js'],
    // loginregistration:['./login/login-register/login-register.spec.js'],
    registerAdditionalInformation: ['./registeradditionalinformation/register-additional-information.spec.js'],
    loginforgotpassword:['./login/login-forgot-password/login-forgot-password.spec.js'],
    directorywithoutlogin: ['./directory/directory-without-login/directory-without-login.spec.js'],
    directorywithlogin: ['./directory/directory-with-login/directory-with-login.spec.js'],
    companypagewithoutlogin: ['./companypage/companypage-without-login/companypage-without-login.spec.js'],
    companypagewithlogin: ['./companypage/companypage-with-login/companypage-with-login.spec.js'],
    companypage: ['./companypage/companypage.spec.js'],
    stockAndInventory: ['./stockandinventory/stockandinventory.spec.js'],
    rental: ['./rental/rental.spec.js'],
     supplieradmindashboard: ['./dashboard/supplier-admin-dashboard/supplier-admin-dashboard.spec.js'],
    // inventory: ['./dashboard/supplier-admin-dashboard/inventory/add-inventory/add-inventory.spec.js'],
    // multipleinventories: ['./dashboard/supplier-admin-dashboard/inventory/add-multiple-inventories/add-multiple-inventories.spec.js'],
    // viewinventory:['./dashboard/supplier-admin-dashboard/inventory/view-inventory/view-inventory.spec.js'],
    // adddirectory: ['./dashboard/supplier-admin-dashboard/directory/add-directory/add-directory.spec.js'],
    // directoryinstructions: ['./dashboard/supplier-admin-dashboard/directory/instructions/instructions.spec.js'],
    // multipledirectories: ['./dashboard/supplier-admin-dashboard/directory/add-multiple-directory/add-multiple-directory.spec.js'],
    // addteam: ['./dashboard/supplier-admin-dashboard/add-team/add-team.spec.js'],
    // messages: ['./dashboard/supplier-admin-dashboard/messages/messages.spec.js'],
    // managecompany: ['./dashboard/supplier-admin-dashboard/manage-company/manage-company.spec.js']
  },

  //maximizes the window and waiting
  onPrepare: function () {
    browser.driver.manage().window().maximize();
    browser.manage().timeouts().implicitlyWait(2500000);
  }

}