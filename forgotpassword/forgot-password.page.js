var Forgotpassword = function(){

    this.enterEmialId=function(data){
        element(by.model('passwordToEmail')).sendKeys(data);
    }

    this.forgotPasswordSubmit=function(){
        element(by.id('forgot-password-submit')).click();
    }

    this.get = async function () {
        browser.get('http://localhost/register/forgot-password?email=livelikeme1986%2Bbuyer27072018@gmail.com');
    };

    this.enterPassword=function(data){
        element(by.model('register.password')).sendKeys(data);
    }

    this.reEnterPassword=function(data){
        element(by.model('register.re_password')).sendKeys(data);
    }

    this.clickOnResetPassword=function(){
        browser.sleep(2000);
        element(by.buttonText('Reset Password')).click();
        browser.sleep(2000);
    }

}
module.exports=new Forgotpassword;