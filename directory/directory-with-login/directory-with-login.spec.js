var Directory=require(__dirname+'/../directory.page.js');
var Directorywithlogin = require(__dirname + '/directory-with-login.page.js');
var Getstarted = require(__dirname + '/../../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../../homepagelogin/homepagelogin.page.js');
var Homepage=require(__dirname+'/../../homepage/homepage.page.js');
var Dashboard=require(__dirname+'/../../dashboard/dashboard.page.js');


describe("Directory Functionality with login", function () {

    it("Open Supplierscave application", function () {
        Getstarted.get();
    })

    it("Enter Username",function(){
        Homepagelogin.userName("livelikeme1986+supplieradmin8@gmail.com");
    })

    it("Enter password",function(){
        Homepagelogin.userPwd('supplierscave');
    })

    it("Click on Login",function(){
        Homepagelogin.userLogin();
    })

    it("Click on Directory",function(){
        Homepage.clickOnDirectory();
    })

    it("Click On Instruments", function () {
        Directory.clickOnPipe();
    })

    it("Click on Stainless India", function () {
        Directory.clickOnStainLessIndia();
    })

    it("Click on Send Enquiry regarding product", function () {
        Directorywithlogin.clickOnSendEnquiry();
    })

    it("Enquiry Subject", function () {
        Directorywithlogin.sendEnquirySubject('With Login Test');
    })

    it("Enquiry Message", function () {
        Directorywithlogin.sendEnquiryMessage('  With Login Test Enquiry Message');
    })

    it("Upload Send Enquiry File", function () {
        Directorywithlogin.uploadFiles();
    })

    it("Send Enquiry", function () {
        Directorywithlogin.sendEnquiry();
    })

    it("Logout from supplieradmin",function(){
        Dashboard.clickLogout();
    })


})