var Directorywithlogin= function(){

    this.clickOnSendEnquiry = function () {
        element(by.id('product-item-send-enquiry')).click();
    }

    this.sendEnquirySubject = function (data) {
        element(by.model('send_enquiry_subject')).sendKeys(data);
    }

    this.sendEnquiryMessage = function (data) {
        element(by.id('ui-tinymce-1_ifr')).sendKeys(data);
    }

    this.uploadFiles = function () {
        var path = require('path');
        var fileToUpload = '/home/mohansreekanth/Projects/workspace/Supplierscave/protractor-frontend/dashboard/supplier-admin-dashboard/directory/add-directory/test.csv';
        absolutePath = path.resolve(__dirname, fileToUpload);
        element(by.id('send-enquiry-choose-file')).sendKeys(absolutePath);
        element(by.id('send-enquiry-upload-file')).click();
    }

    this.sendEnquiry = function () {
        element(by.id('send-enquiry')).click();
    }




}
module.exports=new Directorywithlogin;
