var Stockandinventory = require(__dirname+'/stockandinventory.page.js');
var Getstarted = require(__dirname+'/../getstarted/getstarted.page.js');
var Home = require(__dirname+'/../homepage/homepage.page.js');

describe("Stock & Inventory Functionality",function(){

    it("Opens Suppliers cave Application",function(){
        Getstarted.get();
    })

    it("Click on Stock&Inventory in Homepage",function(){
        Home.clickOnStockAndInventory();
    })

    it("Click on Suppliers Logo",function(){
        Home.clickOnSiteLogo();
    })
})