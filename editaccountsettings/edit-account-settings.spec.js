var Getstarted = require(__dirname + '/../getstarted/getstarted.page.js');
var Homepagelogin = require(__dirname + '/../homepagelogin/homepagelogin.page.js');
var Myaccount = require(__dirname + '/../myaccount/myaccount.page.js');
var Editaccoutsettings = require(__dirname + '/edit-account-settings.page.js');
var Home = require(__dirname+'/../homepage/homepage.page.js');
var Dashboard = require(__dirname + '/../dashboard/dashboard.page.js');


describe("Edit Account Settings", function () {

    it("Opens supplier cave application", function () {
        Getstarted.get();
    })

    it("Enter UserName", function () {
        Homepagelogin.userName('livelikeme1986+270720182@gmail.com');
    })

    it("Enter Password", function () {
        Homepagelogin.userPwd("supplierscave");
    })

    it("Click On Login", function () {
        Homepagelogin.userLogin();
    })

    it("Click on Account Settings", function () {
        Myaccount.myAcc();
    })

    it("Click on Edit Profile", function () {
        Editaccoutsettings.editProfile();
    })

    it("Click on Buyers Checkbox ",function(){
        Editaccoutsettings.editBuyerCheckbox();
    })

    it("Clear Address for profile",function(){
        Editaccoutsettings.clearAddress();
    })

    it("Enter Address for profile",function(){
        Editaccoutsettings.editAddress('D M M GATE');
    })

    it("Clear City for profile",function(){
        Editaccoutsettings.clearCity();
    })

    it("Enter City for profile",function(){
        Editaccoutsettings.editCity('Yelhanka');
    })

    it("Clear Country for profile",function(){
        Editaccoutsettings.clearCountry();
    })

    it("Enter Country for profile ",function(){
        Editaccoutsettings.editCountry('India');
    })

    it("Clear Country code for the profile",function(){
        Editaccoutsettings.clearCountryCode();
    })

    it("Enter Country-code for profile ",function(){
        Editaccoutsettings.editCountryCode('91');
    })

    it("Clear contact-number for the profile",function(){
        Editaccoutsettings.clearContactNumber();
    })

    it("Enter Contact-number for profile",function(){
        Editaccoutsettings.editContactNumber('9676902876');
    })

    it("Clear Email for profile",function(){
        Editaccoutsettings.clearEmail();
    })

    it("Enter Email for profile",function(){
        Editaccoutsettings.editEmail('livelikeme1986+supplier24072018@gmail.com');
    })

    it("Clear FirstName for Profile",function(){
        Editaccoutsettings.clearFirstName();
    })
 
    it("Enter First-Name for profile",function(){
        Editaccoutsettings.editFirstName('Kumara');
    })

    it("Clear Last Name for profile",function(){
        Editaccoutsettings.clearLastName();
    })

    it("Enter Last-Name for profile",function(){
        Editaccoutsettings.editLastName('Swamy');
    })

    it("Clear Designation for profile",function(){
        Editaccoutsettings.clearDesignation();
    })

    it("Enter Designation for profile",function(){
        Editaccoutsettings.editDesignation("Chief Executive Officer");
    })

    it("Click on Save",function(){
        Editaccoutsettings.clickOnsave();
    })

    it("Click on Suppliers Logo",function(){
        Editaccoutsettings.clickOnSiteLogo();
    })

    it("Logout form Buyer Dashboard",function(){
        Dashboard.clickLogout();
    })


})